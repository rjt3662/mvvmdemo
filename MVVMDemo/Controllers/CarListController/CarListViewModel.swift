//
//  CarListViewModel.swift
//  MVVMDemo
//
//  Created by Rajat Mishra on 9/9/19.
//  Copyright © 2019 Rajat Mishra. All rights reserved.
//

import Foundation

class CarListViewModel {
    
    //MARK: - Properties
    private weak var carListController: CarListController!
    var cars = [Car]()
    
    convenience init(carListVC: CarListController) {
        self.init()
        self.carListController = carListVC
    }
    
    //MARK: - Methods
    func fetchCars(completion: @escaping voidCompletion) {
        CoreDataManager.shared.getCars { (fetchedCars) in
            self.cars = fetchedCars
            completion()
        }
    }
    
    
}
