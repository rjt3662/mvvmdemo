//
//  CarTableCell.swift
//  MVVMDemo
//
//  Created by Rajat Mishra on 9/9/19.
//  Copyright © 2019 Rajat Mishra. All rights reserved.
//

import UIKit

class CarTableCell: UITableViewCell {

    @IBOutlet weak var modelNameLabel: UILabel!
    @IBOutlet weak var manufacturerAndTypeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var car: Car! {
        didSet {
            modelNameLabel.text = car.car_model
            manufacturerAndTypeLabel.text = "\(car.manufacturer ?? "") • \(car.car_type ?? "")"
            priceLabel.text = car.price
            priceLabel.sizeToFit()
        }
    }
    
}
