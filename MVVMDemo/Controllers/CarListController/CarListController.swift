//
//  CarListController.swift
//  MVVMDemo
//
//  Created by Rajat Mishra on 9/9/19.
//  Copyright © 2019 Rajat Mishra. All rights reserved.
//

import UIKit

class CarListController: UITableViewController {

    //MARK: - Properties
    private lazy var carListViewModel = CarListViewModel(carListVC: self)
    private var cars: [Car] {
        return carListViewModel.cars
    }
    
    //MARK: - View Load Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 1))
        tableView.register(CarTableCell.nib, forCellReuseIdentifier: CarTableCell.identifier)
        LoadingView.showLoading()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Constants.loadDummyCars {
            self.carListViewModel.fetchCars {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    LoadingView.hideLoading()
                    self.tableView.reloadSections(IndexSet([0]), with: .automatic)
                })
            }
        }
    }

}

extension CarListController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let carTableCell = tableView.dequeueReusableCell(withIdentifier: CarTableCell.identifier, for: indexPath) as! CarTableCell
        carTableCell.car = cars[indexPath.row]
        return carTableCell
    }
    
}

