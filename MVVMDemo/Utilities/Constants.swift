//
//  Constants.swift
//  MVVMDemo
//
//  Created by Rajat Mishra on 9/9/19.
//  Copyright © 2019 Rajat Mishra. All rights reserved.
//

import Foundation

typealias voidCompletion = (()->Void)
typealias StringAny = [String: Any]

final class Constants {

    static func unwrapString(forValue value: Any?) -> String {
        return value as? String ?? ""
    }
    
    static func loadDummyCars(completion: @escaping voidCompletion) {
        if let carsURL = Bundle.main.url(forResource: "cars", withExtension: "json") {
            do {
                let fileData = try Data(contentsOf: carsURL)
                if let carsDictionaries = try JSONSerialization.jsonObject(with: fileData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [StringAny] {
                    CoreDataManager.shared.insertCars(fromDictionaries: carsDictionaries) {
                        completion()
                    }
                }
            } catch {
                print("Cannot get localization_key_value.json : \(error.localizedDescription)")
            }
        }
    }
    
}
