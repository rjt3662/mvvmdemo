//
//  CoreDataManager.swift
//  MVVMDemo
//
//  Created by Rajat Mishra on 9/9/19.
//  Copyright © 2019 Rajat Mishra. All rights reserved.
//

import Foundation

import Foundation
import CoreData

private enum CoreDataEntity {
    case car
    
    var key: String {
        switch self {
        case .car:
            return "Car"
        }
    }
}

class CoreDataManager {
    
    static let shared = CoreDataManager()
    private let persistentContainer = NSPersistentContainer(name: "MVVMDemo")
    
    private init() {
        persistentContainer.loadPersistentStores { (storeDescription, error) in
            print(storeDescription)
            if let err = error as NSError? {
                fatalError("Unresolved error \(err), \(err.userInfo)")
            }
        }
    }
    
    func getCars(completion: @escaping ((_ allCars: [Car]) -> Void)) {
        var tempCars = [Car]()
        let context = persistentContainer.viewContext
        do {
            tempCars = try context.fetch(Car.fetchRequest())
            completion(tempCars)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func insertCars(fromDictionaries dictionaries: [StringAny], completion: @escaping voidCompletion) {
        let context = persistentContainer.viewContext
        for singleCaseDict in dictionaries {
            if let carEntity = NSEntityDescription.insertNewObject(forEntityName: CoreDataEntity.car.key, into: context) as? Car {
                carEntity.car_type = Constants.unwrapString(forValue: singleCaseDict["car_type"])
                carEntity.car_model = Constants.unwrapString(forValue: singleCaseDict["car_model"])
                carEntity.manufacturer = Constants.unwrapString(forValue: singleCaseDict["manufacturer"])
                carEntity.price = Constants.unwrapString(forValue: singleCaseDict["price"])
            }
        }
        saveContext(completion: completion)
    }
    
    func saveContext(completion: (()->Void)? = nil) {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                if let completion = completion {
                    completion()
                }
            } catch {
                let err = error as NSError
                fatalError("Unresolved error \(err), \(err.userInfo)")
            }
        }
    }
    
    private func deleteAllData(forEntity entity: CoreDataEntity) {
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.key)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else { continue }
                context.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
    
}
